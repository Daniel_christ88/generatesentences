package Model;

import java.util.ArrayList;

public class DCADEData {
    
    private String content;
    private String recB;
    private String tag;
    private String ID;
    private String classID;
    private String pathID;
    private String parentID;
    private String TEC_ID;
    private String CEC_ID;
    private int encoding;
    private String colType;
    private ArrayList<String> data = new ArrayList<>();
    
    public DCADEData() { }
    
    public DCADEData(String content, String recB, String tag, String ID, String classID, String pathID, String parentID, String TEC_ID, String CEC_ID, int encoding, String colType) {
        this.content = content;
        this.recB = recB;
        this.tag = tag;
        this.ID = ID;
        this.classID = classID;
        this.pathID = pathID;
        this.parentID = parentID;
        this.TEC_ID = TEC_ID;
        this.CEC_ID = CEC_ID;
        this.colType = colType;
        this.encoding = encoding;
    }
    
    public DCADEData(String content, String recB, String tag, String ID, String classID, String pathID, String parentID, String TEC_ID, String CEC_ID, int encoding, String colType, ArrayList<String> data) {
        this.content = content;
        this.recB = recB;
        this.tag = tag;
        this.ID = ID;
        this.classID = classID;
        this.pathID = pathID;
        this.parentID = parentID;
        this.TEC_ID = TEC_ID;
        this.CEC_ID = CEC_ID;
        this.encoding = encoding;
        this.colType = colType;
        this.data = data;
    }
    
    public Data parseToData(String field) {
        Data d = new Data();
        
        d.setTemplateID(TEC_ID + "-" + CEC_ID);
        d.setField(field);
        d.setData(data);
        
        return d;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRecB() {
        return recB;
    }

    public void setRecB(String recB) {
        this.recB = recB;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getPathID() {
        return pathID;
    }

    public void setPathID(String pathID) {
        this.pathID = pathID;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public String getTEC_ID() {
        return TEC_ID;
    }

    public void setTEC_ID(String TEC_ID) {
        this.TEC_ID = TEC_ID;
    }

    public String getCEC_ID() {
        return CEC_ID;
    }

    public void setCEC_ID(String CEC_ID) {
        this.CEC_ID = CEC_ID;
    }

    public int getEncoding() {
        return encoding;
    }

    public void setEncoding(int encoding) {
        this.encoding = encoding;
    }

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    public ArrayList<String> getData() {
        return data;
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
    }
}
