package Model;

public abstract class Relationship {
    
    private int relationship;
    
    public int getRelationship() {
        return relationship;
    }

    public void setRelationship(int relationship) {
        this.relationship = relationship;
    }
    
    public String getRelationshipString() {
        String sRelationship;
        
        switch (relationship) {
            case 1:
                sRelationship = "1:1";
                break;
            case 2:
                sRelationship = "1:m";
                break;
            case 3:
                sRelationship = "m:1";
                break;
            case 4:
                sRelationship = "m:m";
                break;
            default:
                sRelationship = "-";
                break;
        }
        
        return sRelationship;
    }
}
