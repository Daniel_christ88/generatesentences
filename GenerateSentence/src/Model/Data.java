package Model;

import java.util.ArrayList;

public class Data {
    
    private String templateID;
    private String field;
    private ArrayList<String> data = new ArrayList<>();
    
    public Data() {}
    
    public Data(String templateID, String content) {
        this.templateID = templateID;
        this.field = content;
    }
    
    public Data(Data copy) {
        this.templateID = copy.getTemplateID();
        this.field = copy.getField();
        this.data = copy.getData();
    }
    
    public Data(String templateID, String content, ArrayList<String> data) {
        this.templateID = templateID;
        this.field = content;
        this.data = data;
    }

    public String getTemplateID() {
        return templateID;
    }

    public void setTemplateID(String templateID) {
        this.templateID = templateID;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public ArrayList<String> getData() {
        return data;
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
    }
}
