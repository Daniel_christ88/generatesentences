package Model;

public class TernaryRelationship extends Relationship {
    
    private int[] dependsOnNum;
    private int dependentColNum;
    private String[] dependsOnName;
    private String dependentColName;
    private boolean allCompositeKey = false;
    
    public TernaryRelationship() { }
    
    public TernaryRelationship(int[] dependsOnNum, String[] dependsOnName) {
        this.dependsOnNum = dependsOnNum;
        this.dependsOnName = dependsOnName;
        this.dependentColNum = -1;
        this.dependentColName = null;
        this.allCompositeKey = true;
        super.setRelationship(1);
    }
    
    public TernaryRelationship(int[] dependsOnNum, int dependentColNum, String[] dependsOnName, String dependentColName, int relationship) {
        this.dependsOnNum = dependsOnNum;
        this.dependentColNum = dependentColNum;
        this.dependsOnName = dependsOnName;
        this.dependentColName = dependentColName;
        this.allCompositeKey = false;
        super.setRelationship(relationship);
    }

    public int[] getDependsOnNum() {
        return dependsOnNum;
    }

    public void setDependsOnNum(int[] dependsOnNum) {
        this.dependsOnNum = dependsOnNum;
    }

    public int getDependentColNum() {
        return dependentColNum;
    }

    public void setDependentColNum(int dependentColNum) {
        this.dependentColNum = dependentColNum;
    }

    public String[] getDependsOnName() {
        return dependsOnName;
    }

    public void setDependsOnName(String[] dependsOnName) {
        this.dependsOnName = dependsOnName;
    }

    public String getDependentColName() {
        return dependentColName;
    }

    public void setDependentColName(String dependentColName) {
        this.dependentColName = dependentColName;
    }

    public boolean isAllCompositeKey() {
        return allCompositeKey;
    }

    public void setAllCompositeKey(boolean allCompositeKey) {
        this.allCompositeKey = allCompositeKey;
    }
}
