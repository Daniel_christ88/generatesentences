package Model;

public class BinaryRelationship extends Relationship{
    
    private int colNum;
    private int withColNum;
    private String colName;
    private String withColName;
    
    public BinaryRelationship() { }
    
    public BinaryRelationship(int colNum, int withColNum, String colName, String withColName, int relationship) {
        this.colNum = colNum;
        this.withColNum = withColNum;
        this.colName = colName;
        this.withColName = withColName;
        super.setRelationship(relationship);
    }

    public int getColNum() {
        return colNum;
    }

    public void setColNum(int colNum) {
        this.colNum = colNum;
    }

    public int getWithColNum() {
        return withColNum;
    }

    public void setWithColNum(int withColNum) {
        this.withColNum = withColNum;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getWithColName() {
        return withColName;
    }

    public void setWithColName(String withColName) {
        this.withColName = withColName;
    }
}
