package Model;

import java.util.ArrayList;

public class Template {
    private String templateId;
    private String content;
    private ArrayList<Integer> leafNodeIndex = new ArrayList<Integer>();

    public Template(String templateId, String content, ArrayList<Integer> leafNodeIndex) {
        this.templateId = templateId;
        this.content = content;
        this.leafNodeIndex = leafNodeIndex;
    }
    
    public Template(){
        
    }

    public String getTemplateId() {
        return templateId;
    }

    public String getContent() {
        return content;
    }

    public ArrayList<Integer> getLeafNodeIndex() {
        return leafNodeIndex;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setLeafNodeIndex(ArrayList<Integer> leafNodeIndex) {
        this.leafNodeIndex = leafNodeIndex;
    }
}
