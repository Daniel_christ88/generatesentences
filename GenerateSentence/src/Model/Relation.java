package Model;

import java.util.HashMap;

public class Relation {
    
    private String columnName;
    private HashMap<Integer, Integer> relationship = new HashMap<>();

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public HashMap<Integer, Integer> getRelationship() {
        return relationship;
    }

    public void setRelationship(HashMap<Integer, Integer> relationship) {
        this.relationship = relationship;
    }
    
    public void addRealtion(int withColumn, int relation) {
        this.relationship.put(withColumn, relation);
    }
    
    public String getRelationshipWithColumn(int withColumn) {
        int relationNumber = this.relationship.get(withColumn);
        String relationship = "-";
        
        switch (relationNumber) {
            case 1:
                relationship = "1:1";
                break;
            case 2:
                relationship = "1:m";
                break;
            case 3:
                relationship = "m:1";
                break;
            case 4:
                relationship = "m:m";
                break;
            default:
                relationship = "-";
                break;
        }
        
        return relationship;
    }
}
