
import Controller.ConcludeRelation;
import Controller.FindPrimaryKey;
import Controller.MakeDataTable;
import Controller.ReadLMTable;
import Controller.ReadLNFeatures;
import Controller.GenerateSentence;
import Controller.MakeRelationTable;
import Controller.PopulateDataTable;
import Controller.ReadExampleTable;
import Model.Data;
import Model.Relation;
import Model.Relationship;
import Model.Template;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class GenerateSentences {
    
    /*      ExAlg data set      */
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/ExAlg/Amazon Cars/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/ExAlg/UEFA Players/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/ExAlg/Amazon Pop Artist/";//problem start with BR
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/ExAlg/UEFANationalTeams/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/ExAlg/Tennis/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/ExAlg/eBay/"; //looping,unorder
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/ExAlg/Baseball Players/"; //re-arragement problem
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/ExAlg/NETFLIX/";  //prob adj, unorder, similar problem with detist (See movie reviews below.)
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/ExAlg/RPM Packages/";  //very worst table different format, in column there is a figure with the same strucure with link
    
    /*      TEX data set        */
        /* Books */
    //Tidak Ada ??? static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Books/RunEx/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Books/www.abebooks.com/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Books/www.awesomebooks.com/";//has inconsisten gap
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Books/www.betterworldbooks.com/";//looping, some of OT or MT become OD or MD, e.g. <BR>
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Books/www.manybooks.net/"; //looping, char similarity
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Books/www.waterstones.com/"; //unorder,marking and align problem
        /* Cars */
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Cars/www.autotrader.com/";//looping, unorder
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Cars/www.carmax.com/";//how to get other set
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Cars/www.carzone.ie/";//unorder
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Cars/www.classiccarsforsale.co.uk/";//looping
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Cars/www.internetautoguide.com/"; //record boundary detection should be from the beginning of segment
        /* Events */
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Events/events.linkedin.com/"; //split error
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Events/www.allconferences.com/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Events/www.mbendi.com/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Events/www.netlib.org/"; 
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Events/www.rdlearning.org.uk/";
        /* Doctors */
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Doctors/doctor.webmd.com/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Doctors/extapps.ama-assn.org/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Doctors/www.dentists.com/"; //the last RT infrequent so leaf nodes after that no pattern
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Doctors/www.drscore.com/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Doctors/www.steadyhealth.com/";
        /* Jobs */
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Jobs/careers.insightintodiversity.com/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Jobs/www.4jobs.com/"; //unorder, patter RT lost
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Jobs/www.6figurejobs.com/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Jobs/www.careerbuilder.com/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Jobs/www.jobofmine.com/";
        /* Movies */
    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Movies/www.albaniam.com/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Movies/www.allmovie.com/"; //record boundary, RD goal
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Movies/www.citwf.com/";
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Movies/www.disneymovieslist.com/";//unorder
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Movies/www.imdb.com/"; //this can solve Nelfix can be solved too, seperate Director:, exit Director: & Directors:, Writer: & Writers:
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Movies/www.soulfilms.com/"; //re-arragement Actors based on ParentID, there 2 other potential Set
        /* RealEstate */
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/RealEstate/realestate.yahoo.com/"; //looping,unroder when mark & merge, record boundary error
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/RealEstate/www.haart.co.uk/";//FP RT, No intersection RT jadi error DR
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/RealEstate/www.homes.com/"; //looping, unorder when mark & merge, too sensitive when check record boundary
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/RealEstate/www.remax.com/"; //unorder, record boundary error, can not cover all
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/RealEstate/www.trulia.com/"; //move use paterns, unorder during alignment
        /* Sports */
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Sports/baseball.playerprofiles.com/"; //kacau balao, header table and contenct is not the same sequence, table without TH tags, very suffer!!    //static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Sports/en.uefa.com/"; //table very suffer
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Sports/en.uefa.com/"; //program error, unorder, table very suffer, OT split, the pattern can not be detected
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Sports/www.atpworldtour.com/"; //table very suffer
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Sports/www.nfl.com/"; //unorder, maybe table, part is not correct
//    static String Folder = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/TEX/Sports/www.soccerbase.com/"; //suffer becauseof table!!, fail mark and merge
    
    // Table Study Case
    static String CTX = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/CTX.txt";
    static String Religion1 = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/Religion 1.txt";
    static String Religion2 = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/Religion 2.txt";
    // Table Study Case Atomic
    static String CTX_atomic = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/CTX Atomic.txt";
    static String Religion1_atomic = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/Religion 1 Atomic.txt";
    static String Religion2_atomic = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/Religion 2 Atomic.txt";
    static String TRY = "D:/IF - Alex/Kerja Praktek/Design Database Using NIAM-ORM Conceptual Schema/Program/DataSet/try.txt";
    
    public static void main(String[] args) {
        File inputFile = new File(Folder + "DCADE2"); // read merged text nodes HTML that seperated by decorative tag
        File[] files;
        ArrayList<Template> dataTemplate = new ArrayList<>();
        
        if (!inputFile.exists()) {
            System.out.println("Folder not found!");
        } else if (inputFile.listFiles().length > 0) {
            //Input
                //Reading the LMTable.tsv
//            ReadLMTable control = new ReadLMTable();
//            try {
//                files = inputFile.listFiles(); //membaca isi folder dalam directory Folder
//                dataTemplate = control.readFile(files); //baca file LMTable.tsv
//            } catch (Exception ex) {
//                System.out.println(ex);
//            }
            
                //Reading the LNFeatures folder
//            ReadLNFeatures ln = new ReadLNFeatures();
//            ArrayList<HashMap<Integer, String>> features = ln.readFileLNFeatures(Folder + "DCADE2/LNFeatures/", dataTemplate.get(0).getLeafNodeIndex().size());

                //Reading the example table
            ReadExampleTable control = new ReadExampleTable();
            ArrayList<Data> dataTable = null;
            try {
                dataTable = control.readExampleTable(CTX);
            } catch (Exception ex) {
                System.out.println(ex);
            }
            
            //Proses
                //Getting the data table
            MakeDataTable mdt = new MakeDataTable();
//            ArrayList<Data> dataTable = mdt.makeDataTable(dataTemplate, features);
            mdt.printDataTable(dataTable);
            
            // Checking for null value or set (if set, populate the data table)
            PopulateDataTable pdt = new PopulateDataTable();
            dataTable = pdt.checkingForNullValueOrSet(dataTable);
            
                //Getting the primary key of the data
            FindPrimaryKey fpk = new FindPrimaryKey();
            ArrayList<Integer> primaryKey = fpk.findPrimaryKey(dataTable);
            
                //Finding each columns relationship with other columns
            MakeRelationTable mrt = new MakeRelationTable();
            ArrayList<Relation> relationTable = mrt.makeRelationTable(dataTable);
            mrt.printRelationTable(relationTable, dataTable);
            
                //Conclude the relationship between columns
            ConcludeRelation cr = new ConcludeRelation();
            ArrayList<Relationship> finalRelationship = cr.concludeTableRelation(primaryKey, relationTable, dataTable);
                
                //Generate Sentences from data table
            GenerateSentence gs = new GenerateSentence();
            gs.createSentecesUsingRelations(dataTable, finalRelationship);
            
            //Output
                
        }
    }
}
