package Controller;

import Model.Data;
import java.util.ArrayList;
import java.util.Scanner;

public class FindPrimaryKey {
    
    public ArrayList<Integer> findPrimaryKey(ArrayList<Data> data) {
        System.out.println("\nSearching for the primary key : ");
        ArrayList<Integer> candidateKey = new ArrayList<>();
        ArrayList<Integer> primaryKey = new ArrayList<>();
        
        // Searching for candidate key using unique value in that columns
        for (int i = 0; i < data.size(); i++) {
            System.out.print("Checking column '" + data.get(i).getField() + "'");
            ArrayList<String> values = new ArrayList<>();
            int counterUniqueValue = 0;
            
            for (int j = 0; j < data.get(i).getData().size(); j++) {
                String currentValue = data.get(i).getData().get(j);
                if (!values.contains(currentValue) && values != null) {
                    counterUniqueValue++;
                    values.add(currentValue);
                }
            }
            
            System.out.print(" - unique value = " + counterUniqueValue);
            if (counterUniqueValue == data.get(i).getData().size()) {
                candidateKey.add(i);
                System.out.println(" -> Inserting into list of candidate key");
            } else {
                System.out.println(" -> Not a candidate key");
            }
        }
        
        printCandidateKey(data, candidateKey);
        
        // Searching the primary by the type
        
        
        // Validation with user
        Scanner userInput = new Scanner(System.in);
        int num = 0;
        if (!candidateKey.isEmpty()) {
            for (int i = 0; i < candidateKey.size(); i++) {
                System.out.println( (i + 1) + ". " + data.get( candidateKey.get(i) ).getField());
            }
            System.out.println("0. I will choose the primary key myself");
            System.out.print("Please, choose the primary key using the number : ");
            
            num = userInput.nextInt();
            userInput.nextLine();
        }
        
        // Getting the user input
        if (num == 0) {
            System.out.println("\nThe columns number and name : ");
            for (int i = 0; i < data.size(); i++) {
                System.out.println( (i + 1) + ". " + data.get(i).getField() );
            }
            
            while (primaryKey.size() == 0) {
                System.out.print("Please input the primary key column number (use seperator ',', if composite key) : ");
                String input = userInput.nextLine();

                String[] arrInput = input.split(",");
                System.out.println("");
                for (String in : arrInput) {
                    // If input is the columns name
//                    for (int j = 0; j < data.size(); j++) {
//                        if (in.trim().toLowerCase().equals( data.get(j).getField().toLowerCase() )) {
//                            primaryKey.add(j);
//                            break;
//                        }
//                    }

                    // If input is the columns number
                    in = in.trim();
                    int numCol = Integer.parseInt(in) - 1;
                    primaryKey.add(numCol);
                }
            }
        } else {
            primaryKey.add( candidateKey.get( num - 1 ) );
        }
        
        printPrimaryKey(data, primaryKey);
        return primaryKey;
    }
    
    public void printCandidateKey(ArrayList<Data> data, ArrayList<Integer> candidateKey) {
        if (candidateKey.isEmpty()) {
            System.out.println("We can't find the candidate key.");
        } else {
            System.out.print("\nCandidate Key : ");
            for (int i = 0; i < candidateKey.size(); i++) {
                if (i == candidateKey.size() - 1) {
                    System.out.println(data.get(candidateKey.get(i)).getField() + "\n");
                } else {
                    System.out.print(data.get(candidateKey.get(i)).getField() + ", ");
                }
            }
        }
    }
    
    public void printPrimaryKey (ArrayList<Data> data, ArrayList<Integer> primaryKey) {
        System.out.print("The primary key that you choose is ");
        for (int i = 0; i < primaryKey.size(); i++) {
            if (i == primaryKey.size() - 1) {
                System.out.println("'" + data.get(primaryKey.get(i)).getField() + "'\n");
            } else {
                System.out.print("'" + data.get(primaryKey.get(i)).getField() + "', ");
            }
        }
    }
}
