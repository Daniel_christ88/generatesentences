package Controller;

import Model.Data;
import Model.Template;
import java.util.ArrayList;
import java.util.HashMap;

public class MakeDataTable {
    
    private ArrayList<HashMap<Integer, String>> features;
    private double success = 0;
    private double failed = 0;
    
    public ArrayList<Data> makeDataTable(ArrayList<Template> template, ArrayList<HashMap<Integer, String>> features) {
        this.features = features;
        ArrayList<Data> arrData = new ArrayList<>();
        
        for (int i = 0; i < template.size() - 1; i++) {
            ArrayList<String> newData = getDataFromGap( template.get(i), template.get(i + 1) );
            
            if (newData != null) {
                Data d = new Data(template.get(i).getTemplateId(), template.get(i).getContent(), newData);
                arrData.add(d);
            }
        }
        
        System.out.println("\nSuccess = " + this.success + " with a Percentage = " + Double.toString((this.success/(this.success+this.failed))*100) + " %");
        System.out.println("Failed = " + this.failed + " with a Percentage = " + Double.toString((this.failed/(this.success+this.failed))*100) + " %");
        System.out.println("");
        return arrData;
    }
    
    ArrayList<String> getDataFromGap(Template front, Template back) {
        ArrayList<String> returnData = new ArrayList<>();
        int counterData = 0;
        
        for (int i = 0; i < front.getLeafNodeIndex().size(); i++) {
            //Gap between two leaf index
            int gap = back.getLeafNodeIndex().get(i) - front.getLeafNodeIndex().get(i) - 1;
            
            if (gap == 0) { //No gap between two leaf index => no data
                String noData = null;
                returnData.add(noData); //Add null to arraylist
            } else if (gap == 1) { //There is one gap between two leaf index => attribute-value pair
                String dataGap = features.get(i).get(gap + front.getLeafNodeIndex().get(i)); //Getting data from document i with leaf node index (front leaf node index + gap)
                returnData.add(dataGap);
                
                counterData++; //Add counter every data found
                this.success++; //count succes get data
            } else if (gap >= 2) {
                String noData = null;
                returnData.add(noData); //Add null to arraylist
                
                this.failed++;
            }
        }
        
        if (counterData != 0) {
            System.out.println("There is data in template " + front.getContent());
            return returnData;
        }
        return null;
    }
    
    public void printDataTable(ArrayList<Data> arrData) {
        System.out.println("Print data table : ");
        
        arrData.forEach((data) -> {
            System.out.print(data.getTemplateID() + "\t");
        });
        System.out.println("");
        
        arrData.forEach((data) -> {
            System.out.print(data.getField() + "\t");
        });
        System.out.println("");
        
        for (int i = 0; i < arrData.get(0).getData().size(); i++) {
            
            for (int j = 0; j < arrData.size(); j++) {
                System.out.print(arrData.get(j).getData().get(i) + "\t");
            }
            System.out.println("");
            
        }
        System.out.println("");
    }
}
