package Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class ReadLNFeatures {
    
    public ArrayList<HashMap<Integer, String>> readFileLNFeatures(String folder, int n) {
        System.out.println("Reading files in LNFeatures");
        ArrayList<HashMap<Integer, String>> features = new ArrayList<>();
        
        for (int i = 1; i <= n; i++) {
            String file = folder + "page-";
            if (i < 10) {
                file += "000" + i + ".txt";
            } else if (i < 100) {
                file += "00" + i + ".txt";
            } else if (i < 1000) {
                file += "0" + i + ".txt";
            } else {
                file += i + ".txt";
            }
            
            File inputFile = new File(file);
            if (!inputFile.exists()) {
                System.out.println("File " + file + " does not exists!");
            } else {
                try {
                    Scanner myReader = new Scanner(inputFile);
                    myReader.nextLine(); //Read the first line
                    HashMap<Integer, String> data = new HashMap<>();
                    
                    //System.out.println("File : " + file);
                    while (myReader.hasNextLine()) {
                        String line = myReader.nextLine();
                        String[] arrLine = line.split("\t");

                        //Getting leaf node index
                        int leafNodeIndex = Integer.parseInt(arrLine[0]);

                        //Getting content of that leaf node index
                        String content = arrLine[12];

                        data.put(leafNodeIndex, content);
                        //System.out.println(leafNodeIndex + " - " + content);
                    }
                    //System.out.println("\n\n");
                    
                    features.add(data);
                } catch (FileNotFoundException ex) {
                    System.out.println("File " + file + " is not found");
                }
            }
        }
        
        System.out.println("Reading files in LNFeatures - finished! \n\n");
        return features;
    }
}
