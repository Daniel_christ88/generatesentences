package Controller;

import Model.Data;
import Model.Relation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MakeRelationTable {
    
    public ArrayList<Relation> makeRelationTable(ArrayList<Data> data) {
        System.out.println("\nFinding the relationship between columns : ");
        ArrayList<Relation> relationTable = new ArrayList<>();
        
        for (int i = 0; i < data.size(); i++) {
            Relation newRelation = new Relation();
            newRelation.setColumnName( data.get(i).getField() );
            
            for (int j = 0; j < data.size(); j++) {
                String withColumn = data.get(j).getField();
                
                if (i == j) {
                    newRelation.addRealtion(j, 0); // We use number 0 to indicate "-" or no relationship
                } else {
                    int relationship = findRelationshipBetweenTwoColumns( data.get(i), data.get(j) );
                    newRelation.addRealtion(j, relationship);
                }
            }
            
            relationTable.add(newRelation);
        }
        return relationTable;
    }
    
    // Function to find relationship between two columns
    int findRelationshipBetweenTwoColumns(Data d1, Data d2) {
        int relationship = 1; // We use number 1 to indicate "1:1" or one-to-one relationship
        HashMap<String, Integer> d1_occurences = new HashMap<>();
        HashMap<String, Integer> d2_occurences = new HashMap<>();
        ArrayList<String> d1_d2_pairs = new ArrayList<>();
        
        // Counting the occurences of data
        for (int i = 0; i < d1.getData().size(); i++) {
            String d1_string = d1.getData().get(i);
            String d2_string = d2.getData().get(i);
            String pair = d1_string + " - " + d2_string;
            
            // If the combination of the two is null and null we can ignore it
            // And when the d1-d2 pair has been counted before we can ingnore it too
            if ( d1_string != null && d2_string != null && !d1_d2_pairs.contains(pair) ) {
                if (d1_occurences.containsKey(d1_string)) {
                    d1_occurences.put( d1_string, d1_occurences.get(d1_string) + 1 );
                } else {
                    d1_occurences.put( d1_string, 1 );
                }
                
                if (d2_occurences.containsKey(d2_string)) {
                    d2_occurences.put( d2_string, d2_occurences.get(d2_string) + 1 );
                } else {
                    d2_occurences.put( d2_string, 1 );
                }
                
                d1_d2_pairs.add(pair);
            }
        }
        
        // Checking the occurences to determine the relationship
        boolean is_d1_only_once = true;
        for (Map.Entry<String, Integer> entry : d1_occurences.entrySet()) {
            if (entry.getValue() != 1) {
                is_d1_only_once = false;
            }
        }
        
        boolean is_d2_only_once = true;
        for (Map.Entry<String, Integer> entry : d2_occurences.entrySet()) {
            if (entry.getValue() != 1) {
                is_d2_only_once = false;
            }
        }
        
        if (is_d1_only_once == false && is_d2_only_once == false) {
            relationship = 4; // We use number 4 to indicate "m:m" or many-to-many relationship
        } else if (is_d1_only_once == true && is_d2_only_once == false) {
            relationship = 2; // We use number 2 to indicate "1:m" or one-to-many relationship
        } else if (is_d1_only_once == false && is_d2_only_once == true) {
            relationship = 3; // We use number 3 to indicate "m:1" or many-to-one relationship
        }
        
        return relationship;
    }
    
    public void printRelationTable(ArrayList<Relation> relationTable, ArrayList<Data> data) {
        System.out.println("Printing the relationship table : ");
        
        for (int i = 0; i < data.size(); i++) {
            System.out.print( "\t" + data.get(i).getField() );
        }
        System.out.println("");
        
        for (int i = 0; i < data.size(); i++) {
            int colNum = i;
            String column = data.get(i).getField();
            System.out.print(column);
            
            relationTable.forEach((r) -> {
                System.out.print("\t" + r.getRelationshipWithColumn(colNum) );
            });
            System.out.println("");
        }
        System.out.println("\n");
    }
}
