package Controller;

import Model.Data;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReadExampleTable {
    
    public ArrayList<Data> readExampleTable(String path) {
        ArrayList<Data> data = new ArrayList<>();
        
        System.out.println("Reading from file with path = " + path);
        try (BufferedReader br = new BufferedReader( new FileReader(path) )) {
            // Read txt per line
            int counter = 0;
            String currentLine;
            while ( (currentLine = br.readLine()) != null ) {
                String[] arrLine = currentLine.split("\t");
                
                if (counter == 0) {
                    for (String words : arrLine) {
                        Data newData = new Data();
                        
                        newData.setTemplateID(null);
                        newData.setField(words);
                        
                        data.add(newData);
                    }
                } else {
                    for (int i = 0; i < arrLine.length; i++) {
                        if (!"".equals(arrLine[i])) {
                            data.get(i).getData().add( arrLine[i] );
                        } else {
                            data.get(i).getData().add(null);
                        }
                    }
                }
                counter++;
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File is not found");
            return null;
        } catch (IOException ex) {
            System.out.println("IO Exception");
            return null;
        }
        System.out.println("Finished reading! \b");
        
        return data;
    }
}
