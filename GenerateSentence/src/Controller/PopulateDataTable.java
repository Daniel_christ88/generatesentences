package Controller;

import Model.Data;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class PopulateDataTable {
    
    public ArrayList<Data> checkingForNullValueOrSet(ArrayList<Data> data) {
        boolean hasNull = false;
        for (int i = 0; i < data.get(0).getData().size(); i++) {
            for (int j = 0; j < data.size(); j++) {
                if ( data.get(j).getData().get(i) == null ) {
                    hasNull = true;
                    break;
                }
            }
            
            if (hasNull) {
                break;
            }
        }
        
        if (!hasNull) {
            return data;
        }
        
        Scanner s = new Scanner(System.in);
        System.out.println("\nData has null value. Is it : ");
        System.out.println("1. It is null value");
        System.out.println("2. Null value because there is a set in the table");
        System.out.print("Please input the number : ");
        int userInput = s.nextInt();
        s.nextLine();
        
        if (userInput == 2) {
            System.out.println("\nTime to populate the data table");
            ArrayList<Data> result = null;
            
            boolean usingSimplePopulationTechnique = isSimplePopulationEnough(data); 
            if (usingSimplePopulationTechnique) {
                result = simplePopulationTechnique(data);
            } else {
                result = advancePopulationTechnique(data);
            }
            
            if (result != null) {
                MakeDataTable mdt = new MakeDataTable();
                mdt.printDataTable(result);
            }
            
            return result;
        }
        
        System.out.println("");
        return data;
    }
    
    boolean isSimplePopulationEnough(ArrayList<Data> data) {
        ArrayList<Integer> pastColumnWithData = new ArrayList<>();
        boolean pastRowFullData = true;
        for (int i = 0; i < data.get(0).getData().size(); i++) {
            boolean currentRowFullData = true;
            boolean moreThanOneColumnWithData = false;
            for (int j = 0; j < data.size(); j++) {
                if ( data.get(j).getData().get(i) == null ) {
                    currentRowFullData = false;                    
                } else {
                    if (pastRowFullData) {
                        pastColumnWithData.add(j);
                    } else {
                        if (!pastColumnWithData.contains(j)) {
                            moreThanOneColumnWithData = true;
                        }
                    }
                }
            }
            
            if (currentRowFullData) {
                pastColumnWithData.clear();
                moreThanOneColumnWithData = false;
            }
            
            if (pastColumnWithData.size() > 1) {
                System.out.println("Cannot fill it with simple population technique.");
//                System.out.println("Past column with data : " + pastColumnWithData.size());
                return false;
            } else if (moreThanOneColumnWithData) {
                System.out.println("Cannot fill it with simple population technique, because it has more than two column with data every null row.");
                return false;
            }
            
            pastRowFullData = currentRowFullData;
        }
        
        System.out.println("Filling the data table using simple population techniques.");
        return true;
    }
    
    ArrayList<Data> simplePopulationTechnique(ArrayList<Data> initialData) {
        ArrayList<Data> result = new ArrayList<>();
        
        // Initialize the result data
        for (int i = 0; i < initialData.size(); i++) {
            Data newData = new Data(initialData.get(i).getTemplateID(), initialData.get(i).getField());
            result.add(newData);
        }
        
        // Populate the null value by getting the last row with full data
        for (int i = 0; i < initialData.get(0).getData().size(); i++) {
            for (int j = 0; j < initialData.size(); j++) {
                if ( initialData.get(j).getData().get(i) == null ) {
                    result.get(j).getData().add( result.get(j).getData().get( i-1 ) );
                } else {
                    result.get(j).getData().add( initialData.get(j).getData().get(i) );
                }
            }
        }
        
        return result;
    }
    
    ArrayList<Data> advancePopulationTechnique(ArrayList<Data> initialData) {
        System.out.println("Because the data table cannot be populate with simple technique, please answer all following questions : \n");
        ArrayList<Data> result = new ArrayList<>();
        
        // In this techniques, we need to get user input to populate the table with correct way
        Scanner s = new Scanner(System.in);
        System.out.println("Are all the columns in data table have ternary relationship?");
        System.out.println("1. Yes, they are ternary relationship with all of the columns.");
        System.out.println("0. No, they aren't.");
        System.out.print("Please input the number : ");
        int allIsTernaryRelationship = s.nextInt();
        s.nextLine();
        System.out.println("");
        
        if ( allIsTernaryRelationship == 1 ) {
            result = simplePopulationTechnique(initialData);
        } else {
            // Initialize the result data and ask the pivot for this table
            for (int i = 0; i < initialData.size(); i++) {
                Data newData = new Data(initialData.get(i).getTemplateID(), initialData.get(i).getField());
                result.add(newData);
                
                System.out.println( (i + 1) + ". " + newData.getField() );
            }
            
            System.out.print("Please input the column number as pivot for populate the table : ");
            int pivotColumn = s.nextInt() - 1;
            s.nextLine();
            
            ArrayList< ArrayList<Integer> > relationship = checkingForTernaryRelationship(initialData, pivotColumn);
            
            // Checking if all columns used in ternary
            ArrayList<Integer> checkColumns = new ArrayList<>();
            if (relationship.size() != 0) {
                for (int i = 0; i < relationship.size(); i++) {
                    for (int j = 0; j < relationship.get(i).size(); j++) {
                        if (!checkColumns.contains( relationship.get(i).get(j) )) {
                            checkColumns.add( relationship.get(i).get(j) );
                        }
                    }
                }
            }
            
            if (initialData.size() != checkColumns.size()) {
                ArrayList< ArrayList<Integer> > binaryRelationship = checkingForBinaryRelationship(initialData, checkColumns, pivotColumn);
                relationship.addAll(binaryRelationship);
            }
            
            result = naturalJoinOfEveryRelationship(initialData, relationship);
        }
        
        return result;
    }
    
    ArrayList< ArrayList<Integer> > checkingForBinaryRelationship(ArrayList<Data> data, ArrayList<Integer> checkColumns, int pivotColumn) {
        System.out.println("Because there are some columns that not included in the ternary relationship, we will ask some more question on binary relationship.");
        ArrayList< ArrayList<Integer> > result = new ArrayList<>();
        
        Scanner s = new Scanner(System.in);
        for (int i = 0; i < data.size(); i++) {
            if (!checkColumns.contains(i) && i != pivotColumn) {
                ArrayList<Integer> binaryRelationship = new ArrayList<>();
                binaryRelationship.add(i);
                
                // Check with the user on binary relationship of this column
                System.out.print("Is " + data.get(pivotColumn).getField() + " - " + data.get(i).getField() + " a binary relationship (1. Yes/ 0. No)? ");
                int isDefaultBinary = s.nextInt();
                s.nextLine();
                
                if (isDefaultBinary == 1) {
                    binaryRelationship.add( pivotColumn );
                } else {
                    System.out.println("\nPlease choose the column that have binary relationship with column " + data.get(i).getField() + " : ");
                    
                    int counter = 1;
                    for (int j = 0; j < data.size(); j++) {
                        if (i != j) {
                            System.out.println( (counter) + ". " + data.get(j).getField() );
                            counter++;
                        }
                    }
                    
                    System.out.print("Please choose the one of number above : ");
                    int columnNumber = s.nextInt();
                    s.nextLine();
                    
                    if (columnNumber <= i) {
                        binaryRelationship.add( columnNumber - 1 );
                    } else {
                        binaryRelationship.add( columnNumber );
                    }
                }
                
                // Adding all relationship to checkColumns
                for (int r : binaryRelationship) {
                    if (!checkColumns.contains(r)) {
                        checkColumns.add(r);
                    }
                }
                
                result.add(binaryRelationship);
            }
        }
        
        if (!checkColumns.contains(pivotColumn)) {
            ArrayList<Integer> binaryRelationship = new ArrayList<>();
            binaryRelationship.add(pivotColumn);
            
            System.out.println("Looks like the pivot column doesn't have any relationship yet.");
            System.out.println("Please input the column that have relationship with pivot column or " + data.get(pivotColumn).getField() + " : ");
            
            int counter = 1;
            for (int i = 0; i < data.size(); i++) {
                if (i != pivotColumn) {
                    System.out.println( counter + ". " + data.get(i).getField() );
                    counter++;
                }
            }
            
            System.out.print("Please input the number of column : ");
            int columnNumber = s.nextInt();
            s.nextLine();
            
            if (columnNumber <= pivotColumn) {
                binaryRelationship.add( columnNumber - 1 );
            } else {
                binaryRelationship.add( columnNumber );
            }
            
            result.add(binaryRelationship);
        }
        
        System.out.println("Finished searching for binary relationship in the data table.");
        return result;
    }
    
    ArrayList< ArrayList<Integer> > checkingForTernaryRelationship(ArrayList<Data> data, int pivotColumn) {
        System.out.println("Checking for ternary relationship in the data table using data...");
        ArrayList<String> columnsCombinations = null;
        ArrayList<Integer> dataCount = new ArrayList<>();
        
        // Initialize the data count for counting data in each columns
        for(int i = 0; i < data.size(); i++) {
            dataCount.add(0);
        }
        
        // Counting data every time pivot column has data
        // If columns has the same data every time pivot has data, that means it maybe has ternary relationship
        for (int i = 0; i < data.get(0).getData().size(); i++) {
            for (int j = 0; j < data.size(); j++) {
                if (data.get(j).getData().get(i) != null) {
                    int counter = dataCount.get(j) + 1;
                    dataCount.set(j, counter);
                }
            }
            
            if ( i == data.get(0).getData().size() - 1 || data.get( pivotColumn ).getData().get( i + 1 ) != null ) {
//                System.out.println( data.get( pivotColumn ).getData().get( i + 1 ) );
                
                ArrayList<String> everyPossibleCombinations = new ArrayList<>();
                for (int k = 0; k < data.size(); k++) {

                    int counterDataK = dataCount.get(k);
                    ArrayList<Integer> columns = new ArrayList<>();
                    columns.add(k);
                    
//                    String print = k + "(" + counterDataK + ") ";
                    for (int l = k + 1; l < data.size(); l++) {
                        if (counterDataK == dataCount.get(l)) {
                            columns.add(l);
//                            print += "- " + l + "(" + dataCount.get(l) + ") ";
                        }
                    }
//                    System.out.println(print);
                    
                    if (columns.size() > 1) {
                        // If there are columns with same data count make every possible combination
                        ArrayList<String> currentPossibleCombinations = makeEveryPossibleCombinationOfColumns(columns);
                        // Add to overall combinations
                        everyPossibleCombinations.addAll(currentPossibleCombinations);
                    }
                }
                
                if (!everyPossibleCombinations.isEmpty()) {
                    
                    if (columnsCombinations == null) {
                        // Add for the first time
                        columnsCombinations = everyPossibleCombinations;

//                        System.out.println("First time combination : ");
//                        for (String combination : columnsCombinations) {
//                            System.out.println(combination);
//                        }
//                        System.out.println("\n");
                    } else {
                        // Check if there are same combination or not
                        // If there is combination that doesn't the same erase it from the columns combination
                        for ( String combination : columnsCombinations ) {
                            if (!everyPossibleCombinations.contains(combination)) {
                                columnsCombinations.remove(combination);
//                                System.out.println("Deleting combination : " + combination);
                            }
                        }
                    }
                }
            }
        }
        
        // Check it with the user for the confirmation of ternary relationship
//        System.out.println(columnsCombinations.size());
        ArrayList< ArrayList<Integer> > result = new ArrayList<>();
        if ( !columnsCombinations.isEmpty() ) {
            System.out.println("We found some candidate for ternary relationship : ");
            for ( String ternaryCombination : columnsCombinations ) {
                String[] ternaryColumns = ternaryCombination.split("-");
//                System.out.println(ternaryCombination);
                
                String ternaryString = "";
                for (int i = 0; i < ternaryColumns.length; i++) {
                    int ternaryColumnsInt = Integer.parseInt( ternaryColumns[i] );
                    ternaryString += data.get( ternaryColumnsInt ).getField() + " - ";
                }
                ternaryString = ternaryString.substring(0, ternaryString.length() - 3);
                
                System.out.println( "1. " + ternaryString + " - " + data.get( pivotColumn ).getField() );
                if (ternaryColumns.length > 2) {
                    System.out.println("2. " + ternaryString);
                }
                System.out.println("0. None of all above");
                
                Scanner s = new Scanner(System.in);
                System.out.print("Please input the number of the answer : ");
                int userInput = s.nextInt();
                s.nextLine();
                System.out.println("");
                
                if (userInput != 0) {
                    ArrayList<Integer> newTernaryCombination = new ArrayList<>();
                    for (String column : ternaryColumns) {
                        int columnInt = Integer.parseInt(column);
                        newTernaryCombination.add(columnInt);
                    }

                    if (userInput == 1) {
                        newTernaryCombination.add(pivotColumn);
                    }
                    
                    result.add(newTernaryCombination);
                }
            }
        }
        
        if (result.isEmpty()) {
            System.out.println("No ternary relationship found in the data table.\n");
        } else {
            System.out.println("Finished searching for ternary relationship in the data table.\n");
        }
        
        return result;
    }
    
    ArrayList<String> makeEveryPossibleCombinationOfColumns(ArrayList<Integer> columns) {
        ArrayList<String> combinations = new ArrayList<>();
        
//        for (int i = 0; i < columns.size(); i++) {
//            String combination = "" + columns.get(i);
//            for (int j = i + 1; j < columns.size(); j++) {
//                combination += "-" + columns.get(j);
//                combinations.add(combination);
//            }
//        }

        for (int i = 2; i <= columns.size(); i++) {
            ArrayList<String> combinationsWithSize = makeCombinationWithCounter(columns, i);
            combinations.addAll(combinationsWithSize);
        }
        
        return combinations;
    }
    
    ArrayList<String> makeCombinationWithCounter(ArrayList<Integer> columnsNumber, int counter) {
        ArrayList<String> combinations = new ArrayList<>();
        
        // Make all possible composite key with columns as many as the counter
        int[] combinationNumber = new int[ counter ];
        for (int i = 0; i < counter; i++) {
            combinationNumber[i] = i;
        }
        
        boolean done = false;
        while (!done) {
            String stringCompositeKey = "";
            for (int i = 0; i < counter; i++) {
                
                if (i == counter - 1) {
                    stringCompositeKey += columnsNumber.get( combinationNumber[i] );
                    combinations.add(stringCompositeKey);
//                    System.out.println(stringCompositeKey);
                } else {
                    stringCompositeKey += columnsNumber.get( combinationNumber[i] ) + "-";
                }
            }
            
            // Increase the counter at the last combination number
            combinationNumber = counterCombinationNumber(combinationNumber, counter, columnsNumber.size());
            
            // Check if the last number in combination number more than it should be
            if (combinationNumber[counter - 1] >= columnsNumber.size()) {
                // Then all the possible combination already done
                done = true;
            }
        }
        
        return combinations;
    }
    
    int[] counterCombinationNumber(int[] combinationNumber, int counter, int max) {
        combinationNumber[counter - 1] = combinationNumber[counter - 1] + 1;
        
        if (combinationNumber[counter - 1] >= max) {
            combinationNumber = counterCombinationNumber( combinationNumber, counter - 1, max);
            
            if (counter - 1 != 0) {
                combinationNumber[counter - 1] = combinationNumber[counter - 2] + 1;
            }
        }
        
        return combinationNumber;
    }
    
    ArrayList<Data> naturalJoinOfEveryRelationship(ArrayList<Data> initialData, ArrayList< ArrayList<Integer> > relationship) {
        ArrayList<Data> result = new ArrayList<>();
        ArrayList<Integer> checkColumnsResult = new ArrayList<>();
        
        MakeDataTable mdt = new MakeDataTable();
        
        // Natural join every relationship
        // By grouping through the relationship first, then populate it, and lastly natural join
        while (!relationship.isEmpty()) {
            if (result.isEmpty()) {
                // Initialize to store relationship that will be natural joined
                ArrayList<Integer> relationship1 = null;
                ArrayList<Integer> relationship2 = null;
                
                for (int i = 0; i < relationship.size() - 1; i++) {
                    for (int j = i + 1; j < relationship.size(); j++) {
                        for (int column : relationship.get(j)) {
                            // The columns that will be natural join must have one column with same column
                            if ( relationship.get(i).contains(column) ) {
                                relationship1 = relationship.get(i);
                                relationship2 = relationship.get(j);
                                
//                                String naturalJoin = "";
//                                for (int r : relationship1) {
//                                    naturalJoin += initialData.get(r).getField() + " - ";
//                                }
//                                naturalJoin = naturalJoin.substring(0, naturalJoin.length() - 3) + " natural join with ";
//                                
//                                for (int r : relationship2) {
//                                    naturalJoin += initialData.get(r).getField() + " - ";
//                                }
//                                naturalJoin = naturalJoin.substring(0, naturalJoin.length() - 3);
//                                
//                                System.out.println(naturalJoin);
                                break;
                            }
                        }
                        
                        if (relationship1 != null) {
                            break;
                        }
                    }
                    
                    if (relationship1 != null) {
                        break;
                    }
                }
                
                // Initialize data from the columns used
                ArrayList<Data> d1 = getDataWithColumns(initialData, relationship1);
                ArrayList<Data> d2 = getDataWithColumns(initialData, relationship2);
                
                // Populate first with simple population technique
                d1 = simplePopulationTechnique(d1);
                d2 = simplePopulationTechnique(d2);
                
//                mdt.printDataTable(d1);
//                mdt.printDataTable(d2);
                
                // Natural join between two data
                result = naturalJoin(d1, d2);
                
                // Add the column that have been used to check column
                for (int r : relationship1) {
                    if (!checkColumnsResult.contains(r)) {
                        checkColumnsResult.add(r);
                    }
                }
                
                for (int r : relationship2) {
                    if (!checkColumnsResult.contains(r)) {
                        checkColumnsResult.add(r);
                    }
                }
                
                // Delete both relationship that already used
                relationship.remove(relationship1);
                relationship.remove(relationship2);
            } else {
                ArrayList<Integer> columnsToBeJoined = null;
                for (ArrayList<Integer> r : relationship) {
                    for (int column : r) {
                        if (checkColumnsResult.contains(column)) {
                            columnsToBeJoined = r;
                            break;
                        }
                    }
                    
                    if (columnsToBeJoined != null) {
                        break;
                    }
                }
                
                // Initialize data from the columns used
                ArrayList<Data> d = getDataWithColumns(initialData, columnsToBeJoined);
                
                // Populate first with simple population technique
                d = simplePopulationTechnique(d);
                
                // Natural join between two data
                result = naturalJoin(result, d);
                
                // Add the column that have been used to check column
                for (int r : columnsToBeJoined) {
                    if (!checkColumnsResult.contains(r)) {
                        checkColumnsResult.add(r);
                    }
                }
                
                // Delete both relationship that already used
                relationship.remove(columnsToBeJoined);
            }
        }
        
        System.out.println("\nFinised natural join of all relationship...");
        return result;
    }
    
    ArrayList<Data> naturalJoin(ArrayList<Data> d1, ArrayList<Data> d2) {
        ArrayList<Data> flattenD1 = removeRepeatedValue(d1);
//        System.out.println("Flatten D1 size = " + flattenD1.get(0).getData().size());
        
        ArrayList<Data> flattenD2 = removeRepeatedValue(d2);
//        System.out.println("Flatten D2 size = " + flattenD2.get(0).getData().size());
        
//        // Print ulang
//        System.out.println("Flatten D1 size = " + flattenD1.get(0).getData().size());
//        System.out.println("Flatten D2 size = " + flattenD2.get(0).getData().size());
        
        ArrayList<Data> result = new ArrayList<>();
        HashMap<String, Integer> mapIndex = new HashMap<>();
        
        // Get the index of pivot column
        ArrayList<Integer> d1Pivot = new ArrayList<>();
        ArrayList<Integer> d2Pivot = new ArrayList<>();
        for (int i = 0; i < flattenD1.size(); i++) {
            for(int j = 0; j < flattenD2.size(); j++) {
                if (flattenD1.get(i).getField().equals( flattenD2.get(j).getField() )) {
                    d1Pivot.add(i);
                    d2Pivot.add(j);
                }
            }
        }
        
        // Initialize the result
        for (Data d : flattenD1) {
            if (!mapIndex.containsKey( d.getField() )) {
                Data newData = new Data( d.getTemplateID(), d.getField() );
                result.add(newData);
                
                mapIndex.put( d.getField(), result.indexOf(newData));
            }
        }
        
        for (Data d : flattenD2) {
            if (!mapIndex.containsKey( d.getField() )) {
                Data newData = new Data( d.getTemplateID(), d.getField() );
                result.add(newData);
                
                mapIndex.put( d.getField(), result.indexOf(newData));
            }
        }
        
        for (int i = 0; i < flattenD1.get( d1Pivot.get(0) ).getData().size(); i++) {
            for (int j = 0; j < flattenD2.get( d2Pivot.get(0) ).getData().size(); j++) {
                boolean pivotHaveSimilarData = true;
                for (int k = 0; k < d1Pivot.size(); k++) {
                    if (!flattenD1.get( d1Pivot.get(k) ).getData().get(i).equals( flattenD2.get( d2Pivot.get(k) ).getData().get(j) )) {
                        pivotHaveSimilarData = false;
                        break;
                    }
                }
                
                if (pivotHaveSimilarData) {
                    for (Data d : flattenD1) {
                        result.get( mapIndex.get(d.getField()) ).getData().add( d.getData().get(i) );
                    }
                    
                    for (int k = 0; k < flattenD2.size(); k++) {
                        if (!d2Pivot.contains(k)) {
                            result.get( mapIndex.get(flattenD2.get(k).getField()) ).getData().add( flattenD2.get(k).getData().get(j) );
                        }
                    }
                }
            }
        }
        
//        System.out.println("Result size = " + result.get(0).getData().size());
        
        return result;
    }
    
    ArrayList<Data> removeRepeatedValue(ArrayList<Data> data) {
//        System.out.println("\nFlattening the data : ");
        ArrayList<Data> flattenData = new ArrayList<>();
        ArrayList<String> pastData = new ArrayList<>();
        
        for (Data d : data) {
            Data newData = new Data(d.getTemplateID(), d.getField());
            flattenData.add(newData);
        }
        
        int i = 0;
        int k = 0;
        while (i < data.get(0).getData().size()) {
            String newString = "";
            
            for (int j = 0; j < data.size(); j++) {
                flattenData.get(j).getData().add( data.get(j).getData().get(i) );
                if (data.get(j).getData().get(i) != null) {
                    if (j == data.size() - 1) {
                        newString += data.get(j).getData().get(i);
                    } else {
                        newString += data.get(j).getData().get(i) + "-";
                    }
                }
            }
            
            if (!pastData.contains(newString)) {
//                System.out.println("String = " + newString);
                pastData.add(newString);
            } else {
//                System.out.print("String = " + newString);
                for (int j = 0; j < flattenData.size(); j++) {
//                    System.out.print(" | Removing = " + flattenData.get(j).getData().get(k));
                    flattenData.get(j).getData().remove(k);
                }
//                System.out.println("");
                
                k--;
            }
            
            i++;
            k++;
//            System.out.println("Current index = " + i);
        }
        
//        System.out.println("Flatten data size = " + flattenData.get(0).getData().size() + "\n");
        return flattenData;
    }
    
    ArrayList<Data> getDataWithColumns(ArrayList<Data> initialData, ArrayList<Integer> columns) {
        ArrayList<Data> data = new ArrayList<>();
        
        // Initialize the initial data
        for (int column : columns) {
            Data newData = new Data();
            newData.setField( initialData.get(column).getField() );
            newData.setTemplateID( initialData.get(column).getTemplateID() );
            data.add(newData);
        }
        
        // Inserting all data from initial data to new data array list
        for (int i = 0; i < initialData.get(0).getData().size(); i++) {
            for (int j = 0; j < data.size(); j++) {
                data.get(j).getData().add( initialData.get( columns.get(j) ).getData().get(i) );
            }
        }
        
        return data;
    }
}
