package Controller;

import Model.Template;
import java.io.File;
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.util.Scanner;
import java.util.ArrayList;

public class ReadLMTable {

    public ArrayList<Template> readFile(File[] listTemplate) {
        ReadLMTable control = new ReadLMTable();
        ArrayList<Template> dataTemplate = new ArrayList<>(); //untuk menampung kumpulan data yang sudah dibaca dari file LMTable
        int row = 1; //untuk menentukan (1)templateId, (2)content, dan (3)leafNodeIndex
        
        File fileTemplate = control.searchFile(listTemplate); //untuk mencari LMTable.tsv
        System.out.println("Nama Path yang diambil : " + fileTemplate); //nanti dihapus yah
        if(fileTemplate != null) {
            try {
                Scanner myReader = new Scanner(fileTemplate); //membaca isi dari fileTemplate (fileTemplate membawa directory sampai titik file yang ingin dibuka) Contoh : C:\Users\User\Documents\Daniel C\Kerja\KMITL\Data Set\TEX\Movies\www.albaniam.com\DCADE2\LMTable.tsv
                while (myReader.hasNextLine()) {
                    String data = myReader.nextLine(); //membaca tiap line dari file LMTable
                    dataTemplate = control.insertIntoArrayList(dataTemplate, data, row); //memasukkan data ke dalam variable
                    row++;                
                }
                control.printDataTemplate(dataTemplate); //print data
                myReader.close();
                return dataTemplate;
            } catch (FileNotFoundException e) {
                System.out.println("An error occurred." + e);
                return null;
            }
        } else {
            return null;
        }
    }
    
    File searchFile(File[] listTemplate){
        for(int i = 0; i < listTemplate.length ; i++) {
            if(listTemplate[i].getName().contains("LMTable.tsv")) {
                return listTemplate[i];
            }
        }
        return null;
    }
    
    ArrayList<Template> insertIntoArrayList(ArrayList<Template> dataTemplate, String data, int row) {
        String[] temp = data.split("\t"); //mensplit berdasarkan tab
        Template template; //initialize Object Template
        
        switch (row) {
            case 1:
                for (int i = 0; i < temp.length; i++) { //memasukkan data templateId ke ArrayList dataTemplate
                    template = new Template();
                    template.setTemplateId(temp[i]);
                    dataTemplate.add(template);
                }   
                break;
            case 2:
                for (int i = 0; i < dataTemplate.size(); i++) { //memasukkan data content ke ArrayList dataTemplate
                    dataTemplate.get(i).setContent(temp[i]);
                }   
                break;
            default:
                for (int i = 0; i < dataTemplate.size(); i++) { //memasukkan data leafNodeIndex ke ArrayList dataTemplate
                    ArrayList<Integer> leafNodeIndex = dataTemplate.get(i).getLeafNodeIndex();
                    leafNodeIndex.add(Integer.parseInt(temp[i]));
                    dataTemplate.get(i).setLeafNodeIndex(leafNodeIndex);
                }   
                break;
        }
        
        return dataTemplate;
    }
    
    void printDataTemplate(ArrayList<Template> dataTemplate) {
        for(int i = 0; i < dataTemplate.size(); i++) {
            System.out.print(dataTemplate.get(i).getTemplateId() + "\t");
        }
        System.out.println("");
        
        for(int i = 0; i < dataTemplate.size(); i++) {
            System.out.print(dataTemplate.get(i).getContent() + "\t");
        }
        System.out.println("");
        
        for(int i = 0; i < dataTemplate.get(i).getLeafNodeIndex().size(); i++) {
            for(int y = 0; y < dataTemplate.size(); y++) {
                System.out.print(dataTemplate.get(y).getLeafNodeIndex().get(i) + "\t");
            }
            System.out.println("");
        }
        System.out.println("Jumlah Content : " + dataTemplate.size());
    }
}
