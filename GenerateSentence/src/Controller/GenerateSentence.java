package Controller;

import Model.BinaryRelationship;
import Model.Data;
import Model.Relationship;
import Model.TernaryRelationship;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class GenerateSentence {
    
    public ArrayList<String> createSentencesUsingBruteForce(ArrayList<Data> dataTable) {
        ArrayList<String> sentences = new ArrayList<>();
        String entityType = null;
        
        //Input Entity Type
        while(entityType == null || entityType.equals("")){
           entityType = JOptionPane.showInputDialog(null, "Input Entity Type : "); 
           if(entityType.equals("")) {
               JOptionPane.showMessageDialog(null, "Entity Type cannot be null && must be string!");
           } 
        }
        
        System.out.println("");
        
        //Create Sentence
        for(int i = 0; i < dataTable.get(0).getData().size(); i++) {
            int pageNumber = i + 1;
            System.out.println("Page number : " + pageNumber);
            
            for(int j = 0; j < dataTable.size() - 1; j++) {
                for(int k = j + 1; k < dataTable.size(); k++) {
                    String sentence = entityType + " with " + dataTable.get(j).getField() + " " + dataTable.get(j).getData().get(i);
                    sentence +=  " has " + dataTable.get(k).getField() + " " + dataTable.get(k).getData().get(i);
                    
                    System.out.println(sentence);
                } 
            }
            
            System.out.println("\n");
            if (i == 0) {
                break;
            }
        }
                
        return null;
    }
    
    public void createSentecesUsingRelations(ArrayList<Data> dataTable, ArrayList<Relationship> relationships) {
        String entityType = null;
        
        //Input Entity Type
//        while(entityType == null || entityType.equals("")){
//           entityType = JOptionPane.showInputDialog(null, "Input Entity Type : "); 
//           if(entityType.equals("")) {
//               JOptionPane.showMessageDialog(null, "Entity Type cannot be null && must be string!");
//           } 
//        }

        Scanner s = new Scanner(System.in);
        System.out.print("Please input the entity name : ");
        entityType = s.nextLine();
        
        System.out.println("\nCreating sentences using relationship between columns : ");
        if (relationships != null) {
            
            for (Relationship r : relationships) {
                if (r instanceof BinaryRelationship) {
                    
                    BinaryRelationship br = (BinaryRelationship) r;
                    System.out.println("\nBinary relationship = " + br.getColName() + " has " + br.getWithColName() + " with relationship " + br.getRelationshipString());
                    
                    ArrayList<String> pastString = new ArrayList<>();
                    for (int i = 0; i < dataTable.get(0).getData().size(); i++) {
                        String print = entityType + " with " + br.getColName() + " '" + dataTable.get( br.getColNum() ).getData().get(i) + "' has " + 
                                br.getWithColName() + " '" + dataTable.get( br.getWithColNum() ).getData().get(i) + "'.";
                        
                        if (!pastString.contains(print)) {
                            System.out.println(print);
                            pastString.add(print);
                        }
                    }
                
                } else if (r instanceof TernaryRelationship) {
                    
                    TernaryRelationship tr = (TernaryRelationship) r;
                    System.out.print("\nTernary relationship = ");
                    if (tr.isAllCompositeKey()) {
                        
                        for (int i = 0; i < tr.getDependsOnName().length; i++) {
                            if (i == tr.getDependsOnName().length - 1) {
                                System.out.println("and " + tr.getDependsOnName()[i] + " is a composite key.");
                            } else {
                                System.out.print(tr.getDependsOnName()[i] + ", ");
                            }
                        }
                        
                        ArrayList<String> pastString = new ArrayList<>();
                        for (int i = 0; i < dataTable.get(0).getData().size(); i++) {
                            String print = entityType + " with ";
                            
                            for (int j = 0; j < tr.getDependsOnName().length; j++) {
                                if (j == tr.getDependsOnName().length - 1) {
                                    print += "and " + tr.getDependsOnName()[j] + " '" + 
                                            dataTable.get( tr.getDependsOnNum()[j] ).getData().get(i) + "' is a composite key.";
                                } else {
                                    print += tr.getDependsOnName()[j] + " '" + 
                                            dataTable.get( tr.getDependsOnNum()[j] ).getData().get(i) + "', ";
                                }
                            }
                            
                            if (!pastString.contains(print)) {
                                System.out.println(print);
                                pastString.add(print);
                            }
                        }
                        
                    } else {
                        
                        for (int i = 0; i < tr.getDependsOnName().length; i++) {
                            if (i == tr.getDependsOnName().length - 1) {
                                System.out.println("and " + tr.getDependsOnName()[i] + " affects " + tr.getDependentColName());
                            } else {
                                System.out.print(tr.getDependsOnName()[i] + ", ");
                            }
                        }
                        
                        ArrayList<String> pastString = new ArrayList<>();
                        for (int i = 0; i < dataTable.get(0).getData().size(); i++) {
                            String print = entityType + " with ";
                            
                            for (int j = 0; j < tr.getDependsOnName().length; j++) {
                                if (j == tr.getDependsOnName().length - 1) {
                                    print += "and " + tr.getDependsOnName()[j] + " '" + 
                                            dataTable.get( tr.getDependsOnNum()[j] ).getData().get(i) 
                                            + "' has " + tr.getDependentColName() + " '" + 
                                            dataTable.get( tr.getDependentColNum() ).getData().get(i) + "'.";
                                } else {
                                    print += tr.getDependsOnName()[j] + " '" + 
                                            dataTable.get( tr.getDependsOnNum()[j] ).getData().get(i) + "', ";
                                }
                            }
                            
                            if (!pastString.contains(print)) {
                                System.out.println(print);
                                pastString.add(print);
                            }
                        }
                        
                    }
                    
                }
            }
            
        } else {
            System.out.println("There is no relationship between columns");
        }
    }
}
