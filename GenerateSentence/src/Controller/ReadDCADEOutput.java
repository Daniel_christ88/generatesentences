package Controller;

import Model.DCADEData;
import Model.Data;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReadDCADEOutput {
    
    public ArrayList<Data> readDCADEOutput(String link) {
        ArrayList<Data> data = new ArrayList<>();
        ArrayList<DCADEData> dataDCADE = readTableA(link);
        
        
        
        return data;
    }
    
    public ArrayList<DCADEData> readTableA(String link) {
        ArrayList<DCADEData> data = new ArrayList<>();
        
        String pathTableA = link + "TableA.txt";
        System.out.println("Reading from folder : " + link);
        try (BufferedReader br = new BufferedReader( new FileReader(pathTableA) )) {
            
            // Read txt per line
            int counter = 0;
            String currentLine;
            while ( (currentLine = br.readLine()) != null) {
                String[] arrLine = currentLine.split("\t");
                
                if (arrLine.length > 1) {
                    if (counter == 0) {
                        // For Content and initialize the DCADEData
                        for (int i = 1; i < arrLine.length; i++) {
                            DCADEData dcadeData = new DCADEData();
                            dcadeData.setContent( arrLine[i] );
                            data.add(dcadeData);
                        }
                    } else if (counter == 1) {
                        // For Rec B
                        for (int i = 1; i < arrLine.length; i++) {
                            int forArrayList = i - 1;
                            if ( !"".equals( arrLine[i] ) ) {
                                data.get( forArrayList ).setRecB( arrLine[i] );
                            } else {
                                data.get( forArrayList ).setRecB( null );
                            }
                        }
                    } else if (counter == 2) {
                        // For Tag
                        for (int i = 1; i < arrLine.length; i++) {
                            int forArrayList = i - 1;
                            if ( !"".equals( arrLine[i] ) ) {
                                data.get( forArrayList ).setTag( arrLine[i] );
                            } else {
                                data.get( forArrayList ).setTag( null );
                            }
                        }
                    } else if (counter == 3) {
                        // For ID
                        for (int i = 1; i < arrLine.length; i++) {
                            int forArrayList = i - 1;
                            if ( !"".equals( arrLine[i] ) ) {
                                data.get( forArrayList ).setID( arrLine[i] );
                            } else {
                                data.get( forArrayList ).setID( null );
                            }
                        }
                    } else if (counter == 4) {
                        // For Class ID
                        for (int i = 1; i < arrLine.length; i++) {
                            int forArrayList = i - 1;
                            if ( !"".equals( arrLine[i] ) ) {
                                data.get( forArrayList ).setClassID( arrLine[i] );
                            } else {
                                data.get( forArrayList ).setClassID( null );
                            }
                        }
                    } else if (counter == 5) {
                        // For Path ID
                        for (int i = 1; i < arrLine.length; i++) {
                            int forArrayList = i - 1;
                            if ( !"".equals( arrLine[i] ) ) {
                                data.get( forArrayList ).setPathID(arrLine[i] );
                            } else {
                                data.get( forArrayList ).setPathID( null );
                            }
                        }
                    } else if (counter == 6) {
                        // For Parent ID
                        for (int i = 1; i < arrLine.length; i++) {
                            int forArrayList = i - 1;
                            if ( !"".equals( arrLine[i] ) ) {
                                data.get( forArrayList ).setParentID( arrLine[i] );
                            } else {
                                data.get( forArrayList ).setParentID( null );
                            }
                        }
                    } else if (counter == 7) {
                        // For TEC ID
                        for (int i = 1; i < arrLine.length; i++) {
                            int forArrayList = i - 1;
                            if ( !"".equals( arrLine[i] ) ) {
                                data.get( forArrayList ).setTEC_ID( arrLine[i] );
                            } else {
                                data.get( forArrayList ).setTEC_ID( null );
                            }
                        }
                    } else if (counter == 8) {
                        // For CEC ID
                        for (int i = 1; i < arrLine.length; i++) {
                            int forArrayList = i - 1;
                            if ( !"".equals( arrLine[i] ) ) {
                                data.get( forArrayList ).setCEC_ID( arrLine[i] );
                            } else {
                                data.get( forArrayList ).setCEC_ID( null );
                            }
                        }
                    } else if (counter == 9) {
                        // For Encoding
                        for (int i = 1; i < arrLine.length; i++) {
                            int forArrayList = i - 1;
                            if ( !"".equals( arrLine[i] ) ) {
                                data.get( forArrayList ).setEncoding( Integer.parseInt(arrLine[i]) );
                            } else {
                                data.get( forArrayList ).setEncoding( -1 );
                            }
                        }
                    } else if (counter == 10) {
                        // For Col Type
                        for (int i = 1; i < arrLine.length; i++) {
                            int forArrayList = i - 1;
                            if ( !"".equals( arrLine[i] ) ) {
                                data.get( forArrayList ).setColType( arrLine[i] );
                            } else {
                                data.get( forArrayList ).setColType( null );
                            }
                        }
                    } else {
                        // For Data
                        for (int i = 1; i < arrLine.length; i++) {
                            int forArrayList = i - 1;
                            if ( !"".equals( arrLine[i] ) ) {
                                data.get( forArrayList ).getData().add( arrLine[i] );
                            } else {
                                data.get( forArrayList ).getData().add( null );
                            }
                        }
                    }
                }
                
                counter++;
            }
                    
        } catch (FileNotFoundException ex) {
            System.out.println("FIle is not found");
            return null;
        } catch (IOException ex) {
            System.out.println("IO Exception");
            return null;
        }
        
        return data;
    }
}
