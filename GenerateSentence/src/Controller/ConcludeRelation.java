package Controller;

import Model.BinaryRelationship;
import Model.Data;
import Model.Relation;
import Model.Relationship;
import Model.TernaryRelationship;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ConcludeRelation {
    
    public ArrayList<Relationship> concludeTableRelation(ArrayList<Integer> primaryKey, ArrayList<Relation> relations, ArrayList<Data> data) {
        System.out.println("Concluding the relationship between columns...  ");
        ArrayList<Relationship> finalRelationship = new ArrayList<>();
        ArrayList<Integer> finishedColumns = new ArrayList<>();
        ArrayList<String> pastRelationship = new ArrayList<>();
        ArrayList<Integer> columnsWithManyToManyRelationship = new ArrayList<>();
        Scanner s = new Scanner(System.in);
        
        // Picking the first primary key as initial pivot
        int pk = primaryKey.get(0);
        int numCol = 0;
        
        while (numCol < relations.size()) {
            
            int withColumn;
            int relationship;
            
            // We can skip relationship between column of primary key with itself
            if (numCol != pk && !finishedColumns.contains(numCol)) {
                withColumn = pk;
                relationship = relations.get(numCol).getRelationship().get(withColumn);
                
                for (int j = 0; j < relations.size(); j++) {
                    // Check if the pair already been selected before
                    if (!pastRelationship.contains( relations.get(numCol).getColumnName() + "-" + relations.get(j).getColumnName() )) {
                        
                        // If the column have relationship with higher priority, change the with column and the relationship
                        if ( relations.get(numCol).getRelationship().get(j) < relationship // Check if the new relationship have higher priority than last relationship
                                && relations.get(numCol).getRelationship().get(j) != 0 ) { // Relationship must not 0 or "-" which means no relationship
                            withColumn = j;
                            relationship = relations.get(numCol).getRelationship().get(withColumn);
                        } else if ( relations.get(numCol).getRelationship().get(j) == relationship // If the new relationship have the same priority with the last relationship
                                && withColumn != pk
                                && primaryKey.contains(j) ) { // But with one of the primary key. We choose this one
                            withColumn = j;
                        }
                        
                    }
                }
                
                System.out.println( "Relation betweeen column '" + relations.get(numCol).getColumnName() 
                                    + "' with column '" + relations.get(withColumn).getColumnName() + "' is " 
                                    + relations.get(numCol).getRelationshipWithColumn(withColumn) + " relationship.");
                
                // Save the relationship so it wouldn't repeated
                pastRelationship.add(relations.get(numCol).getColumnName() + "-" + relations.get(withColumn).getColumnName());
                pastRelationship.add(relations.get(withColumn).getColumnName() + "-" + relations.get(numCol).getColumnName());
                
                boolean repeatThis = false;
                if (withColumn == pk) {
                    finishedColumns.add(numCol);
                } else {
                    // Ask user which column will be the pivot column in this relationship
                    System.out.println("1. " + relations.get(numCol).getColumnName());
                    System.out.println("2. " + relations.get(withColumn).getColumnName());
                    System.out.print("++ Please select the pivot column from the relationship (input the number) : ");
                    int userInput = s.nextInt();
                    System.out.println("");
                    
                    if (userInput == 1) {
                        finishedColumns.add(withColumn);
                        repeatThis = true; // Repeat this column to checked the relationship with the primary key
                    } else {
                        finishedColumns.add(numCol);
                    }
                }
                
                if (relationship == 4 && withColumn == pk) {
                    columnsWithManyToManyRelationship.add(numCol);
                } else {
                    //Add to array list of relationship
                    Relationship newBinaryRelationship = new BinaryRelationship(numCol, withColumn, 
                            relations.get(numCol).getColumnName(), relations.get(withColumn).getColumnName(), 
                            relationship);
                    
                    finalRelationship.add(newBinaryRelationship);
                }
                
                if (repeatThis) {
                    numCol--;
                }
            }
            
            numCol++;
        }
        
        // If there are more than 2 (include the primary key) many to many relationship that means 2 possibilities
        // 1. MVD (Multivalued dependency) or
        // 2. Ternary relationship
        if (columnsWithManyToManyRelationship.size() + 1 > 2) {
            columnsWithManyToManyRelationship.add(pk);
            ArrayList<Integer> columnsNumber = new ArrayList<>();
            System.out.print("\nChecking MVD or ternary in columns : ");
            for (int i = 0; i < columnsWithManyToManyRelationship.size(); i++) {
                columnsNumber.add( columnsWithManyToManyRelationship.get(i) );
                if (i == columnsWithManyToManyRelationship.size() - 1) {
                    System.out.println( "and " + data.get( columnsWithManyToManyRelationship.get(i) ).getField());
                } else {
                    System.out.print( data.get( columnsWithManyToManyRelationship.get(i) ).getField() + ", ");
                }
            }
            
            // Make the brute force / every possible way of binary relationship
            ArrayList<Integer[]> binaryColumns = new ArrayList<>();
            for (int i = 0; i < columnsWithManyToManyRelationship.size() - 1; i++) {
                for (int j = i + 1; j < columnsWithManyToManyRelationship.size(); j++) {
                    Integer[] newBinaryColumn = { columnsWithManyToManyRelationship.get(i), columnsWithManyToManyRelationship.get(j) };
                    binaryColumns.add(newBinaryColumn);
                }
            }
            
            ArrayList<Relationship> mvdOrTernaryRelationship = checkingMVDAndTernaryRelationship(binaryColumns, data, columnsNumber);
            finalRelationship.addAll( mvdOrTernaryRelationship );
        }
        
//        printArrayListOfRelationship(finalRelationship);
        
        return finalRelationship;
    }
    
    ArrayList<Relationship> checkingMVDAndTernaryRelationship(ArrayList<Integer[]> columns, ArrayList<Data> initialData, ArrayList<Integer> columnsNumber) {
        ArrayList<Relationship> result = null;
        
        for (int i = 0; i < columns.size(); i++) {
            // Initialize the first columns
            Integer[] first = columns.get(i);
            
            ArrayList<Integer> indexCheck = new ArrayList<>();
            ArrayList<Data> currentData = new ArrayList<>();
            for (Integer j : first) {
                indexCheck.add(j);
                
                Data copyData = new Data( initialData.get(j) );
                currentData.add( copyData );
            }
//            System.out.println("First : " + first[0] + "-" + first[1]);
            
            // Using recursive function to search all possibilities
            result = checkingMVDRecursively(columns, initialData, columnsNumber, indexCheck, i+1, currentData, null);
            if (result != null) {
                Relationship r = null;
                if (first.length == 2) {
                    r = new BinaryRelationship(first[0], first[1], 
                            initialData.get(first[0]).getField(), initialData.get(first[1]).getField(), 4);
                } else {
                    // It's a ternary relationship
//                    r = new TernaryRelationship();
                }
                result.add(r);
                break;
            }
        }
        
        if (result == null) {
            // Check for ternary relationship
            System.out.println("No suitable binary multivalued dependency, trying to check ternary relationship...");
            result = checkingTernaryRelationship(initialData, columnsNumber);
        }
        
//        printArrayListOfRelationship(result);
        
        return result;
    }
    
    ArrayList<Relationship> checkingMVDRecursively(ArrayList<Integer[]> columns, ArrayList<Data> initialData, ArrayList<Integer> columnsNumber, 
            ArrayList<Integer> indexCheck, int currentPosition, ArrayList<Data> currentData, ArrayList<Relationship> currentRelationship) {
        ArrayList<Relationship> result = currentRelationship;
        
        if (currentPosition >= columns.size()) {
            
            return null;
            
        } else {
            
            Integer[] columnsPosition = columns.get(currentPosition);
            boolean haveSimilarColumn = false;
            boolean haveDifferentColumn = false;
            ArrayList<Integer> newIndexCheck = makeCopyOfArrayListInteger(indexCheck);
            ArrayList<Data> toBeJoined = new ArrayList<>();
            
            for (Integer i : columnsPosition) {
                if (indexCheck.contains(i)) {
                    haveSimilarColumn = true;
                }
                
                if (!indexCheck.contains(i)) {
                    haveDifferentColumn = true;
                    newIndexCheck.add(i);
                }
                
                Data copyData = new Data( initialData.get(i) );
                toBeJoined.add( copyData );
            }
            
            if (haveSimilarColumn && haveDifferentColumn) {
//                System.out.println("Natural Join with columns : " + columnsPosition[0] + "-" + columnsPosition[1]);
                ArrayList<Data> joinedData = naturalJoin(currentData, toBeJoined);
                
                Relationship r = null;
                if (columnsPosition.length == 2) {
                    r = new BinaryRelationship(columnsPosition[0], columnsPosition[1], 
                            initialData.get(columnsPosition[0]).getField(), initialData.get(columnsPosition[1]).getField(), 4);
                } else {
                    // It's a ternary relationship
//                    r = new TernaryRelationship();
                }
                
                if (result == null) {
                    result = new ArrayList<>();
                }
                result.add(r);
                
//                String print = "";
//                for (int col : newIndexCheck) {
//                    print += col + " - ";
//                }
//                System.out.println( print.substring(0, print.length() - 3) );
                
//                System.out.println( newIndexCheck.size() + " | " + columnsNumber.size() );
                if ( isTwoArrayListOfIntegerSimilar(newIndexCheck, columnsNumber) && joinedData.get(0).getData().size() == initialData.get(0).getData().size() ) {
                    joinedData = changePosition(initialData, joinedData, columnsNumber);
                    ArrayList<Data> newInitialDataPosition = changeInitialDataPosition(initialData, columnsNumber);
                    
                    if ( isTwoDataSimilar(newInitialDataPosition, joinedData) ) {
                        return result;
                    } else {
//                        System.out.println("Two data is not similar, go to another natural join combination\n");
                        return checkingMVDRecursively(columns, initialData, columnsNumber, indexCheck, currentPosition+1, currentData, currentRelationship);
                    }
                    
                } else if (joinedData.get(0).getData().size() < initialData.get(0).getData().size() && newIndexCheck.size() < indexCheck.size()) {
                    
                    return checkingMVDRecursively(columns, initialData, columnsNumber, newIndexCheck, currentPosition+1, joinedData, result);
                    
//                    ArrayList<Relationship> temp = checkingMVDRecursively(columns, initialData, columnsNumber, newIndexCheck, currentPosition+1, joinedData, result);
//                    if (temp == null) {
//                        return checkingMVDRecursively(columns, initialData, columnsNumber, indexCheck, currentPosition+1, currentData, currentRelationship);
//                    } else {
//                        return temp;
//                    }
                    
                } else {
                    
//                    System.out.println("Two data is not similar, go to another natural join combination\n");
                    return checkingMVDRecursively(columns, initialData, columnsNumber, indexCheck, currentPosition+1, currentData, currentRelationship);
                    
                }
                
            } else {
                return checkingMVDRecursively(columns, initialData, columnsNumber, indexCheck, currentPosition+1, currentData, currentRelationship);
            }
            
        }
    }
    
    ArrayList<Relationship> checkingTernaryRelationship(ArrayList<Data> initialData, ArrayList<Integer> columnsNumber) {
        ArrayList<Relationship> result = null;
        
        for (int i = 2; i < columnsNumber.size(); i++) {
            // Call the function to test ternary relationship with composite key gradually increasing
            result = testingTernary(initialData, columnsNumber, i);
            
            if (result != null) {
                break;
            }
        }
        
        // If all possibilities of composite key still wrong that means all the columns are composite key
        if (result == null) {
            result = new ArrayList<>();
            
            int[] colNums = new int[columnsNumber.size()];
            String[] colNames = new String[columnsNumber.size()];
            for (int i = 0; i < columnsNumber.size(); i++) {
                colNums[i] = columnsNumber.get(i);
                colNames[i] = initialData.get( columnsNumber.get(i) ).getField();
            }
            
            Relationship newRelationship = new TernaryRelationship(colNums, colNames);
            result.add(newRelationship);
        }
        
        return result;
    }
    
    ArrayList<Relationship> testingTernary(ArrayList<Data> initialData, ArrayList<Integer> columnsNumber, int counter) {
        ArrayList<Relationship> result = new ArrayList<>();
        ArrayList< ArrayList<Integer> > compositeKeys = new ArrayList<>();
        ArrayList<String> compositeKeysName = new ArrayList<>();
//        ArrayList< ArrayList<Integer> > compositeKeysRelationships = new ArrayList<>(); 
        
        // Make all possible composite key with columns as many as the counter
        int[] combinationNumber = new int[ counter ];
        for (int i = 0; i < counter; i++) {
            combinationNumber[i] = i;
        }
        
        boolean done = false;
        while (!done) {
            // Add new composite key combination
            ArrayList<Integer> newCompositeKey = new ArrayList<>();
            String stringCompositeKey = "";
            for (int i = 0; i < counter; i++) {
                newCompositeKey.add( columnsNumber.get( combinationNumber[i] ) );
                
                if (i == counter - 1) {
                    stringCompositeKey += initialData.get( columnsNumber.get( combinationNumber[i] ) ).getField();
                    compositeKeysName.add(stringCompositeKey);
//                    System.out.println(stringCompositeKey);
                } else {
                    stringCompositeKey += initialData.get( columnsNumber.get( combinationNumber[i] ) ).getField() + " - ";
                }
            }
            compositeKeys.add(newCompositeKey);
            
            // Increase the counter at the last combination number
            combinationNumber = counterCombinationNumber(combinationNumber, counter, columnsNumber.size());
            // Check if the last number in combination number more than it should be
            if (combinationNumber[counter - 1] >= columnsNumber.size()) {
                // Then all the possible combination already done
                done = true;
            }
        }
        
        // Make the relationship table between the composite key and other columns
        System.out.println("\nMaking the relationship table of composite key : ");
        for (int numCol : columnsNumber) {
            System.out.print("\t" + initialData.get( numCol ).getField());
        }
        System.out.println("");
        
        ArrayList<Integer[]> ternaryTaken = new ArrayList<>();
        ArrayList<Integer> indexCheck = new ArrayList<>();
        ArrayList< ArrayList<Integer> > candidateNumber = new ArrayList<>();
        HashMap<Integer, ArrayList< ArrayList<Integer> >> mapCandidateNumber = new HashMap<>();
        for (int i = 0; i < compositeKeys.size(); i++) {
//            ArrayList<Integer> compositeKeyRelationship = new ArrayList<>();
            System.out.print(compositeKeysName.get(i) + "\t");
            
            for (int numCol : columnsNumber) {
                if (compositeKeys.get(i).contains(numCol)) {
//                    compositeKeyRelationship.add(0);
                    System.out.print( getStringRelationship(0) + "\t" );
                } else {
                    int relationship = getRelationshipBetweenColumnAndCompositeKey(compositeKeys.get(i), numCol, initialData);
//                    compositeKeyRelationship.add(relationship);
                    System.out.print( getStringRelationship(relationship) + "\t" );
                    
                    if (relationship == 2) {
                        ArrayList<Integer> candidate = new ArrayList<>();
                        ArrayList<Integer> sortedCandidate = new ArrayList<>();
                        for (int compositeKey : compositeKeys.get(i)) {
                            candidate.add(compositeKey);
                            sortedCandidate.add(compositeKey);
                        }
                        candidate.add(numCol);
                        sortedCandidate.add(numCol);
                        
                        Collections.sort(sortedCandidate);
                        
                        if (candidateNumber.isEmpty()) {
                            candidateNumber.add(sortedCandidate);
                            
                            ArrayList< ArrayList<Integer> > forMap = new ArrayList<>();
                            forMap.add(candidate);
                            mapCandidateNumber.put(0, forMap);
                        } else {
                            
                            boolean haveSimilarCandidate = false;
                            for (int j = 0; j < candidateNumber.size(); j++) {
                                if (sortedCandidate.equals( candidateNumber.get(j) )) {
                                    ArrayList< ArrayList<Integer> > forMap = mapCandidateNumber.get(j);
                                    forMap.add(candidate);
                                    mapCandidateNumber.put(j, forMap);
                                    
                                    haveSimilarCandidate = true;
                                    break;
                                }
                            }
                            
                            if (!haveSimilarCandidate) {
                                candidateNumber.add(sortedCandidate);
                            
                                ArrayList< ArrayList<Integer> > forMap = new ArrayList<>();
                                forMap.add(candidate);
                                mapCandidateNumber.put(candidateNumber.indexOf(sortedCandidate), forMap);
                            }
                            
                        }
                    } else if (relationship == 1) { // I am not sure for one-to-one relationship of ternary
                        // If the relationship 1:1, we will go for the ternary without waiting
                        int[] finalCompositeKeys = new int[counter + 1];
                        Integer[] addOns = new Integer[counter + 1];
                        String[] finalCompositeKeysName = new String[counter + 1];
                        for (int j = 0; j < compositeKeys.get(i).size(); j++) {
                            finalCompositeKeys[j] = compositeKeys.get(i).get(j);
                            finalCompositeKeysName[j] = initialData.get( compositeKeys.get(i).get(j) ).getField();
                            addOns[j] = compositeKeys.get(i).get(j);
                            
                            // Add the index to array list so no duplicate occurs
                            indexCheck.add( compositeKeys.get(i).get(j) );
                        }
                        finalCompositeKeys[counter] = numCol;
                        finalCompositeKeysName[counter] = initialData.get( numCol ).getField();
                        
                        Relationship newRelationship = new TernaryRelationship(finalCompositeKeys, finalCompositeKeysName);
                        result.add(newRelationship);
                        
                        ternaryTaken.add(addOns);
                    }
                }
            }
            
//            compositeKeysRelationships.add(compositeKeyRelationship);
            System.out.println("");
        }
        
        for (int i = 0; i < candidateNumber.size(); i++) {
            // Check if the candidate has new columns that haven't picked up yet
            boolean haveDifferentColumn = false;
            for (int colNum : candidateNumber.get(i)) {
                if (!indexCheck.contains(colNum)) {
                    haveDifferentColumn = true;
                    break;
                }
            }
            
            if (haveDifferentColumn) {
                // Check if there is more than one possible composite key
                if (mapCandidateNumber.get(i).size() > 1) {
                    
                    // If more than one, try to get user input to solve it
                    Scanner s = new Scanner(System.in);
                    
                    // Take User input
                    System.out.println("\nPlease decide which is the ternary relationship that you prefers : ");
                    ArrayList< ArrayList<Integer> > cc = mapCandidateNumber.get(i);
                    for (int j = 0; j < cc.size(); j++) {
                        int a = j;
                        System.out.print( (a+1) + ". " );
                        for (int k = 0; k < cc.get(0).size(); k++) {
                            if (k == cc.get(0).size() - 1) {
                                System.out.println( " affect " + initialData.get( cc.get(j).get(k) ).getField() );
                            } else if (k == 0) {
                                System.out.print( initialData.get( cc.get(j).get(k) ).getField() );
                            } else {
                                System.out.print( " - " + initialData.get( cc.get(j).get(k) ).getField() );
                            }
                        }
                    }
                    System.out.print("Please input the number only : ");
                    int userInput = s.nextInt();
                    System.out.println("");
                    
                    // Add it into the relationship
                    ArrayList<Integer> getCandidateNumber = cc.get( userInput - 1 );
                    
                    int[] dependsOnNum = new int[ getCandidateNumber.size() - 1 ];
                    Integer[] addOns = new Integer[counter + 1];
                    String[] dependsOnName = new String[ getCandidateNumber.size() - 1 ];
                    int dependentNum = 0;
                    String dependentName = "";
                    for (int j = 0; j < getCandidateNumber.size(); j++) {
                        if (j == getCandidateNumber.size() - 1) {
                            dependentNum = getCandidateNumber.get(j);
                            dependentName = initialData.get( getCandidateNumber.get(j) ).getField();
                        } else {
                            dependsOnNum[j] = getCandidateNumber.get(j);
                            dependsOnName[j] = initialData.get( getCandidateNumber.get(j) ).getField();
                        }
                        
                        if ( !indexCheck.contains( getCandidateNumber.get(j) ) ) {
                            indexCheck.add(getCandidateNumber.get(j));
                        }
                        
                        addOns[j] = getCandidateNumber.get(j);
                    }
                    
                    Relationship newRelationship = new TernaryRelationship(dependsOnNum, dependentNum, dependsOnName, dependentName, 2);
                    result.add(newRelationship);
                    ternaryTaken.add(addOns);
                    
                } else {
                    
                    // If only one just add it into the relationship
                    ArrayList<Integer> getCandidateNumber = mapCandidateNumber.get(i).get(0);
                    
                    int[] dependsOnNum = new int[ getCandidateNumber.size() - 1 ];
                    Integer[] addOns = new Integer[counter + 1];
                    String[] dependsOnName = new String[ getCandidateNumber.size() - 1 ];
                    int dependentNum = 0;
                    String dependentName = "";
                    for (int j = 0; j < getCandidateNumber.size(); j++) {
                        if (j == getCandidateNumber.size() - 1) {
                            dependentNum = getCandidateNumber.get(j);
                            dependentName = initialData.get( getCandidateNumber.get(j) ).getField();
                        } else {
                            dependsOnNum[j] = getCandidateNumber.get(j);
                            dependsOnName[j] = initialData.get( getCandidateNumber.get(j) ).getField();
                        }
                        
                        if ( !indexCheck.contains( getCandidateNumber.get(j) ) ) {
                            indexCheck.add(getCandidateNumber.get(j));
                        }
                        
                        addOns[j] = getCandidateNumber.get(j);
                    }
                    
                    Relationship newRelationship = new TernaryRelationship(dependsOnNum, dependentNum, dependsOnName, dependentName, 2);
                    result.add(newRelationship);
                    ternaryTaken.add(addOns);
                    
                }
            }
        }
        
        if (result.size() == 0) {
            return null;
        } else {
            if (indexCheck.size() == columnsNumber.size()) {
                return result;
            } else {
                
                // Check using MVD (natural join with the ternary taken)
                // And binary of remaining columns
                for (int i = 0; i < columnsNumber.size(); i++) {
                    if (!indexCheck.contains( columnsNumber.get(i) )) {
                        // Pair it with other columns
                        for (int j = 0; j < columnsNumber.size(); j++) {
                            if (i != j) {
                                Integer[] newBinaryCombination = new Integer[2];
                                newBinaryCombination[0] = columnsNumber.get(j);
                                newBinaryCombination[1] = columnsNumber.get(i);
                                
                                ternaryTaken.add(newBinaryCombination);
                            }
                        }
                    }
                }
                
                ArrayList<Relationship> resultOfNaturalJoin = checkingMVDAndTernaryRelationship(ternaryTaken, initialData, columnsNumber);
                if (resultOfNaturalJoin == null) {
                    return null;
                } else {
                    result.addAll(resultOfNaturalJoin);
                    return result;
                }
                
            }
        }
    }
    
    int[] counterCombinationNumber(int[] combinationNumber, int counter, int max) {
        combinationNumber[counter - 1] = combinationNumber[counter - 1] + 1;
        
        if (combinationNumber[counter - 1] >= max) {
            combinationNumber = counterCombinationNumber( combinationNumber, counter - 1, max);
            
            if (counter - 1 != 0) {
                combinationNumber[counter - 1] = combinationNumber[counter - 2] + 1;
            }
        }
        
        return combinationNumber;
    }
    
    int getRelationshipBetweenColumnAndCompositeKey(ArrayList<Integer> compositeKey, int numCol, ArrayList<Data> data) {
        int relationship = 1; // We use number 1 to indicate "1:1" or one-to-one relationship
        HashMap<String, Integer> d1_occurences = new HashMap<>();
        HashMap<String, Integer> d2_occurences = new HashMap<>();
        ArrayList<String> d1_d2_pairs = new ArrayList<>();
        
        // Counting the occurences of data
        for (int i = 0; i < data.get(numCol).getData().size(); i++) {
            String d1_string = "";
            for (int j = 0; j < compositeKey.size(); j++) {
                if (j == compositeKey.size() - 1 && data.get( compositeKey.get(j) ).getData().get(i) != null) {
                    d1_string += data.get( compositeKey.get(j) ).getData().get(i);
                } else if (data.get( compositeKey.get(j) ).getData().get(i) != null) {
                    d1_string += data.get( compositeKey.get(j) ).getData().get(i) + " - ";
                } else {
                    return 0;
                }
            }
            
            String d2_string = data.get(numCol).getData().get(i);
            String pair = d1_string + " - " + d2_string;
            
            // If the combination of the two is null and null we can ignore it
            // And when the d1-d2 pair has been counted before we can ingnore it too
            if ( d1_string != null && d2_string != null && !d1_d2_pairs.contains(pair) ) {
                if (d1_occurences.containsKey(d1_string)) {
                    d1_occurences.put( d1_string, d1_occurences.get(d1_string) + 1 );
                } else {
                    d1_occurences.put( d1_string, 1 );
                }
                
                if (d2_occurences.containsKey(d2_string)) {
                    d2_occurences.put( d2_string, d2_occurences.get(d2_string) + 1 );
                } else {
                    d2_occurences.put( d2_string, 1 );
                }
                
                d1_d2_pairs.add(pair);
            }
        }
        
        // Checking the occurences to determine the relationship
        boolean is_d1_only_once = true;
        for (Map.Entry<String, Integer> entry : d1_occurences.entrySet()) {
            if (entry.getValue() != 1) {
                is_d1_only_once = false;
            }
        }
        
        boolean is_d2_only_once = true;
        for (Map.Entry<String, Integer> entry : d2_occurences.entrySet()) {
            if (entry.getValue() != 1) {
                is_d2_only_once = false;
            }
        }
        
        if (is_d1_only_once == false && is_d2_only_once == false) {
            relationship = 4; // We use number 4 to indicate "m:m" or many-to-many relationship
        } else if (is_d1_only_once == true && is_d2_only_once == false) {
            relationship = 2; // We use number 2 to indicate "1:m" or one-to-many relationship
        } else if (is_d1_only_once == false && is_d2_only_once == true) {
            relationship = 3; // We use number 3 to indicate "m:1" or many-to-one relationship
        }
        
        return relationship;
    }
    
    String getStringRelationship(int relationship) {
        switch (relationship) {
            case 0:
                return "-";
            case 1:
                return "1:1";
            case 2:
                return "1:m";
            case 3:
                return "m:1";
            case 4:
                return "m:m";
            default:
                return "-";
        }
    }
    
    ArrayList<Data> naturalJoin(ArrayList<Data> d1, ArrayList<Data> d2) {
        ArrayList<Data> flattenD1 = removeRepeatedValue(d1);
//        System.out.println("Flatten D1 size = " + flattenD1.get(0).getData().size());
        
        ArrayList<Data> flattenD2 = removeRepeatedValue(d2);
//        System.out.println("Flatten D2 size = " + flattenD2.get(0).getData().size());
        
//        // Print ulang
//        System.out.println("Flatten D1 size = " + flattenD1.get(0).getData().size());
//        System.out.println("Flatten D2 size = " + flattenD2.get(0).getData().size());
        
        ArrayList<Data> result = new ArrayList<>();
        HashMap<String, Integer> mapIndex = new HashMap<>();
        
        // Get the index of pivot column
        ArrayList<Integer> d1Pivot = new ArrayList<>();
        ArrayList<Integer> d2Pivot = new ArrayList<>();
        for (int i = 0; i < flattenD1.size(); i++) {
            for(int j = 0; j < flattenD2.size(); j++) {
                if (flattenD1.get(i).getField().equals( flattenD2.get(j).getField() )) {
                    d1Pivot.add(i);
                    d2Pivot.add(j);
                }
            }
        }
        
        // Initialize the result
        for (Data d : flattenD1) {
            if (!mapIndex.containsKey( d.getField() )) {
                Data newData = new Data( d.getTemplateID(), d.getField() );
                result.add(newData);
                
                mapIndex.put( d.getField(), result.indexOf(newData));
            }
        }
        
        for (Data d : flattenD2) {
            if (!mapIndex.containsKey( d.getField() )) {
                Data newData = new Data( d.getTemplateID(), d.getField() );
                result.add(newData);
                
                mapIndex.put( d.getField(), result.indexOf(newData));
            }
        }
        
        for (int i = 0; i < flattenD1.get( d1Pivot.get(0) ).getData().size(); i++) {
            for (int j = 0; j < flattenD2.get( d2Pivot.get(0) ).getData().size(); j++) {
                boolean pivotHaveSimilarData = true;
                for (int k = 0; k < d1Pivot.size(); k++) {
                    if (!flattenD1.get( d1Pivot.get(k) ).getData().get(i).equals( flattenD2.get( d2Pivot.get(k) ).getData().get(j) )) {
                        pivotHaveSimilarData = false;
                        break;
                    }
                }
                
                if (pivotHaveSimilarData) {
                    for (Data d : flattenD1) {
                        result.get( mapIndex.get(d.getField()) ).getData().add( d.getData().get(i) );
                    }
                    
                    for (int k = 0; k < flattenD2.size(); k++) {
                        if (!d2Pivot.contains(k)) {
                            result.get( mapIndex.get(flattenD2.get(k).getField()) ).getData().add( flattenD2.get(k).getData().get(j) );
                        }
                    }
                }
            }
        }
        
//        System.out.println("Result size = " + result.get(0).getData().size());
        
        return result;
    }
    
    ArrayList<Data> removeRepeatedValue(ArrayList<Data> data) {
//        System.out.println("\nFlattening the data : ");
        ArrayList<Data> flattenData = new ArrayList<>();
        ArrayList<String> pastData = new ArrayList<>();
        
        for (Data d : data) {
            Data newData = new Data(d.getTemplateID(), d.getField());
            flattenData.add(newData);
        }
        
        int i = 0;
        int k = 0;
        while (i < data.get(0).getData().size()) {
            String newString = "";
            
            for (int j = 0; j < data.size(); j++) {
                flattenData.get(j).getData().add( data.get(j).getData().get(i) );
                if (data.get(j).getData().get(i) != null) {
                    if (j == data.size() - 1) {
                        newString += data.get(j).getData().get(i);
                    } else {
                        newString += data.get(j).getData().get(i) + "-";
                    }
                }
            }
            
            if (!pastData.contains(newString)) {
//                System.out.println("String = " + newString);
                pastData.add(newString);
            } else {
//                System.out.print("String = " + newString);
                for (int j = 0; j < flattenData.size(); j++) {
//                    System.out.print(" | Removing = " + flattenData.get(j).getData().get(k));
                    flattenData.get(j).getData().remove(k);
                }
//                System.out.println("");
                
                k--;
            }
            
            i++;
            k++;
//            System.out.println("Current index = " + i);
        }
        
//        System.out.println("Flatten data size = " + flattenData.get(0).getData().size() + "\n");
        return flattenData;
    }
    
    ArrayList<Data> changePosition(ArrayList<Data> initialData, ArrayList<Data> dataToBeChange, ArrayList<Integer> position) {
        ArrayList<Data> newDataPosition = new ArrayList<>();
        
        for (int pos : position) {
            String fieldName = initialData.get(pos).getField();
            
            for (Data d : dataToBeChange) {
                if (fieldName.equals( d.getField() )) {
//                    System.out.println( "Column name : " + d.getField() + ", with size : " + d.getData().size() );
                    newDataPosition.add(d);
                }
            }
        }
        
        return newDataPosition;
    }
    
    ArrayList<Data> changeInitialDataPosition(ArrayList<Data> initialData, ArrayList<Integer> position) {
        ArrayList<Data> newPosition = new ArrayList<>();
        
        for (int pos : position) {
            newPosition.add( initialData.get(pos) );
        }
        
        return newPosition;
    }
    
    ArrayList<String> convertDataToString(ArrayList<Data> data) {
        ArrayList<String> s = new ArrayList<>();
        
        for (int i = 0; i < data.get(0).getData().size(); i++) {
            String newString = "";
            
            for (int j = 0; j < data.size(); j++) {
                if (data.get(j).getData().get(i) != null) {
                    if (j == data.size() - 1) {
                        newString += data.get(j).getData().get(i);
                    } else {
                        newString += data.get(j).getData().get(i) + "-";
                    }
                }
            }
            
//            System.out.println(newString);
            s.add(newString);
        }
        
        return s;
    }
    
    boolean isTwoDataSimilar(ArrayList<Data> from, ArrayList<Data> to) {
        System.out.println("Checking if the natural joined data similar with initial data...");

//        System.out.println("\nConverting data to string from...");
        ArrayList<String> fromString = convertDataToString(from);

//        System.out.println("\nConverting data to string to...");
        ArrayList<String> toString = convertDataToString(to);
        
        for (String s : fromString) {
            if (toString.contains(s)) {
                toString.remove(s);
            } else {
                System.out.println("Different from the initial data\n");
                return false;
            }
        }
        
        System.out.println("Same as initial data\n");
        return true;
    }
    
    ArrayList<Integer> makeCopyOfArrayListInteger(ArrayList<Integer> from) {
        ArrayList<Integer> copy = new ArrayList<>();
        
        for (Integer i : from) {
            copy.add(i);
        }
        
        return copy;
    }
    
    boolean isTwoArrayListOfIntegerSimilar(ArrayList<Integer> from, ArrayList<Integer> to) {
        if (from.size() == to.size()) {
            
            for (int i : from) {
                if (!to.contains(i)) {
                    return false;
                }
            }
            
            return true;
        }
        
        return false;
    }
    
    void printArrayListOfRelationship(ArrayList<Relationship> relationships) {
        System.out.println("Final relationship : ");
        
        if (relationships != null) {
            
            for (Relationship r : relationships) {
                if (r instanceof BinaryRelationship) {
                    
                    BinaryRelationship br = (BinaryRelationship) r;
                    System.out.println("Binary relationship = " + br.getColName() + " has " + br.getWithColName() + " with relationship " + br.getRelationshipString());
                
                } else if (r instanceof TernaryRelationship) {
                    
                    TernaryRelationship tr = (TernaryRelationship) r;
                    System.out.print("Ternary relationship = ");
                    if (tr.isAllCompositeKey()) {
                        
                        for (int i = 0; i < tr.getDependsOnName().length; i++) {
                            if (i == tr.getDependsOnName().length - 1) {
                                System.out.println("and " + tr.getDependsOnName()[i] + " is a composite key.");
                            } else {
                                System.out.print(tr.getDependsOnName()[i] + ", ");
                            }
                        }
                        
                    } else {
                        
                        for (int i = 0; i < tr.getDependsOnName().length; i++) {
                            if (i == tr.getDependsOnName().length - 1) {
                                System.out.println("and " + tr.getDependsOnName()[i] + " affects " + tr.getDependentColName());
                            } else {
                                System.out.print(tr.getDependsOnName()[i] + ", ");
                            }
                        }
                        
                    }
                    
                }
            }
            
        } else {
            System.out.println("There is no relationship between columns");
        }
    }
}
